#!/bin/bash

# Check if root!
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Uninstall old versions
PKG_OK=$(dpkg-query -W --showformat='${Status}\n' docker|grep "install ok installed")
if [ "" != "$PKG_OK" ]; then
	apt remove docker
fi 

PKG_OK=$(dpkg-query -W --showformat='${Status}\n' docker-engine|grep "install ok installed")
if [ "" != "$PKG_OK" ]; then
        apt remove docker-engine
fi 

PKG_OK=$(dpkg-query -W --showformat='${Status}\n' docker.io|grep "install ok installed")
if [ "" != "$PKG_OK" ]; then
	apt remove docker.io
fi

# Install the requirements
apt update && apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

# Add Docker official GPG key.
rep=$(curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -)

# Add the Docker stable repo
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Install the latest version of Docker CE.
apt update && apt install -y docker-ce

# Verify that Docker CE is installed correctly by running the hello-world image.
docker run hello-world